# Simple multinomial based forecast with Dirichelt prior with alpha = 1.

# Objective is to design a model that finds the probability of a consumer sentiment falling into a particular bucket

#

# This will be a multi stage function.

# need to make a function that can take the sum of the events all bucket factorial divided by the product of each
# bucket total factorial, M = sum(m_1, ...,m_n). M!/(m_1! * ... * m_n!)
import numpy as np


def log_sum(number):
    list_of_logs = np.log(np.arange(1, number + 1))
    sum_of_logs = sum(list_of_logs)
    return sum_of_logs


def multiplicity(list_of_numbers):
    total = sum(list_of_numbers)
    num = log_sum(total)
    den = sum(map(log_sum, list_of_numbers))
    value = np.exp(num - den)
    return value


def prediction_model(predict, data):
    list_of_totals = np.add(predict, data)
    num = sum(map(log_sum, list_of_totals))
    total = sum(predict) + sum(data) + 1
    den = log_sum(total)
    return value
    value = multiplicity(predict) * multiplicity(data) * (np.exp(log_sum(len(data) - 1))) * (sum(data) + 1) * np.exp(num - den)
